const Board = require('../models/board')

exports.post = async  function (req, res) {
  const board = new Board(req.body)
  board.save(err => {
    if (err) throw err
    res.status(201).send(board)
  })
}

exports.getAll = async function (req, res) {
  Board.find((err, boards) => {
    if (err) throw err
    res.status(200).send(boards)
  })
}

exports.delete = async function (req, res) {
  Board.findByIdAndRemove(req.params.id, (err, board) => {
    if (err) throw err
    return res.status(200).send(board)
  })
}
