const express = require('express')
const router = express.Router()

const boardController = require('./controllers/board-controller')

router.get('/boards', boardController.getAll)
router.get('/boards/:id')
router.post('/boards', boardController.post)
router.delete('/boards/:id', boardController.delete)

module.exports = router